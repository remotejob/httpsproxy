package proxyhandler

import (
	"bytes"
	"encoding/json"
	"net/http"

	"gitlab.com/remotejob/httpsproxy/internal/domains"
)

func Sendtochatproxynlp(url string, payload domains.AskFromJs) (domains.AnswertoJs, error) {

	var result domains.AnswertoJs

	bytesRepresentation, err := json.Marshal(payload)
	if err != nil {

		return result, err
	}
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(bytesRepresentation))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json,text/plain, */*")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {

		return result, err
	}
	defer resp.Body.Close()

	json.NewDecoder(resp.Body).Decode(&result)

	return result, nil

}
func Sendtoimgserver(url string) ([]domains.Photo, error) {

	var result []domains.Photo
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json,text/plain, */*")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {

		return result, err
	}
	defer resp.Body.Close()

	// bodyBytes, err := ioutil.ReadAll(resp.Body)
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// bodyString := string(bodyBytes)
	// log.Println(bodyString)

	json.NewDecoder(resp.Body).Decode(&result)

	return result, nil

}

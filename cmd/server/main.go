package main

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/adrianosela/certcache"
	"github.com/adrianosela/sslmgr"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gitlab.com/remotejob/httpsproxy/internal/domains"
	"gitlab.com/remotejob/httpsproxy/pkg/proxyhandler"
	"golang.org/x/crypto/acme/autocert"
)

func Chatask(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var ask domains.AskFromJs

	err := json.NewDecoder(r.Body).Decode(&ask)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	log.Println(ask)
	// http://104.131.122.192:8001/api img
	// {"nguid":"","chatuuid":"63a25ac3-8526-4666-96c4-bf66743d6b15","name":"Venla","age":25,"city":"Rovaniemi","phone":"0600414117","text":"kskssk"}

	// http://165.227.71.179:7000/v1/api/chat

	// {"Answer":"Moro-moro, miten menee."}

	payload := domains.AskFromJs{ask.Nguid, ask.Chatuuid, ask.Name,ask.Phone, ask.Age, ask.City, ask.Text, ask.Mob}

	answertojs, err := proxyhandler.Sendtochatproxynlp("http://165.227.71.179:7000/v1/api/chat", payload)
	if err != nil {
		log.Panicln(err)
	}

	json.NewEncoder(w).Encode(&answertojs)

}

func GetImages(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	images,err :=proxyhandler.Sendtoimgserver("http://104.131.122.192:8001/api")
	if err !=nil {
		log.Panicln(err)
	}
	json.NewEncoder(w).Encode(&images)

}


func main() {

	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"}, // All origins

	})

	h := mux.NewRouter()
	h.Methods(http.MethodGet).Path("/healthcheck").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("Im alive!"))
	})

	h.HandleFunc("/v1/api/chat", Chatask).Methods("POST")
	h.HandleFunc("/api", GetImages).Methods("GET")

	handlercors := c.Handler(h)

	ss, err := sslmgr.NewServer(sslmgr.ServerConfig{
		Hostnames: []string{"ippayment.info"},
		HTTPPort:  ":80",
		HTTPSPort: ":443",
		Handler:   handlercors,
		// ServeSSLFunc: func() bool {
		// 	return strings.ToLower(os.Getenv("PROD")) == "true"
		// },
		CertCache: certcache.NewLayered(
			certcache.NewLogger(),
			autocert.DirCache("."),
		),
		ReadTimeout:         5 * time.Second,
		WriteTimeout:        5 * time.Second,
		IdleTimeout:         25 * time.Second,
		GracefulnessTimeout: 5 * time.Second,
		GracefulShutdownErrHandler: func(e error) {

			log.Fatal(e)
		},
	})

	if err != nil {
		log.Fatal(err)
	}

	ss.ListenAndServe()

	// ss, err := sslmgr.NewSecureServer(handler, "ippayment.info")
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// ss.ListenAndServe()
}

package domains

type AskFromJs struct {
	Nguid    string `json:"Nguid"`
	Chatuuid string `json:"Chatuuid"`
	Phone    string `json:"Phone"`
	Name     string `json:"name"`
	Age      int    `json:"age"`
	City     string `json:"city"`
	Text     string `json:"Text"`
	Mob      int    `json:"Mob"`
}

type Ask struct {
	Nguid    string `json:"nguid"`
	Chatuuid string `json:"chatuuid"`
	Text     string `json:"text"`
	Mob      int    `json:"mob"`
}

type AnswertoJs struct {

	Answer string `json:"Answer"`
}

type Photo struct {
	Id          int `json:"Id"`
	Name        string `json:"Name"`
	Age         int `json:"Age"` 
	Phone       string `json:"Phone"`
	City        string `json:"City"`
	Imgfilename string `json:"Img_file_name"`

}